//[SECTION] Dependencies and Modules
	
	const Course = require('../models/Course');

//[SECTION] Functionalities [CREATE]
	
	module.exports.createCourse = (info) => {
		let cName = info.name;
		let cDesc = info.description;
		let cCost = info.price;
		let newCourse = new Course ({
			name: cName,
			description: cDesc,
			price: cCost
		}) 
		return newCourse.save().then((savedCourse, error) => {
			if (error) {
				return 'Failed to Save New Document'
			} else {
				return savedCourse;
			}
		});
	};

//[SECTION] Functionalities [RETRIEVE]
	
	//[SUB-SECTION] [GET ALL COURSE]

		module.exports.getAllCourse = () => {

			return Course.find({}).then(result => {
				return result;
			});
		};

	//[SUB-SECTION] [GET SINGLE COURSE]

		module.exports.getCourse = (id) => {
			
			return Course.findById(id).then(resultOfQuery => {
				return resultOfQuery;
			})
		};
	
	//[SUB-SECTION] [GET ALL ACTIVE COURSE]

		module.exports.getAllActiveCourses = () => {
			
			return Course.find({isActive: true}).then(resultOfTheQuery => {
				return resultOfTheQuery;
			});
		};

//[SECTION] Functionalities [UPDATE]
	
	//[SUB-SECTION] [UPDATE COURSE]

		module.exports.updateCourse = (id, details) => {
		
			let cName = details.name;
			let cDesc = details.description;
			let cCost = details.price;
			
			let updatedCourse = {
				name: cName,
				description: cDesc,
				price: cCost
			}
			
			return Course.findByIdAndUpdate(id, updatedCourse).then((courseUpdated, err) => {
				
				if (err) {
					return 'Failed to update Course';
				} else {
					return 'Successully Updated Course';
				}
			})
		}

	//[SUB-SECTION] [DEACTIVATE COURSE]

		module.exports.deactivateCourse = (id) => {
			
			let updates = {
				isActive: false
			}
			
			return Course.findByIdAndUpdate(id, updates).then((archived, err) => {
				
				if (archived) {
					return `The Course ${id} has been deactivated`;
				} else {
					return `Failed to archive course`
				}
			})
		}

	//[SUB-SECTION] [REACTIVATE COURSE] [**ACTIVITY**]

		module.exports.reactivateCourse = (id) => {
			
			let updates = {
				isActive: true
			}
			
			return Course.findByIdAndUpdate(id, updates).then((archived, err) => {
				
				if (archived) {
					return `The Course ${id} is now Active`;
				} else {
					return `Failed to activate course`
				}
			})
		}

//[SECTION] Functionalities [DELETE]
	
	module.exports.deleteCourse = (id) => {
		
		return Course.findByIdAndRemove(id).then((removedCourse, err) => {

			if (err) {
				return 'No Course Was Removed';
			} else {
				return 'Course Successfully Deleted';
			};

		})
	}