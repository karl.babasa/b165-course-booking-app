//[SECTION] Dependencies and Module

	const User = require('../models/User');
	const Course = require("../models/Course")
	const bcrypt = require("bcrypt");
	const dotenv = require("dotenv");
	const auth = require("../auth");

//[SECTION] Environment Setup

	dotenv.config();
	const salt = parseInt(process.env.SALT);

//[SECTION] Functionalities [CREATE]

	//[SUB-SECTION] [CREATE USER]

	module.exports.registerUser = (data) => {

		let fName = data.firstName;
		let lName = data.lastName;
		let email = data.email;
		let passW = data.password;
		let gendr = data.gender;
		let mobil = data.mobileNumber;
		
		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, salt),
			gender: gendr,
			mobileNumber: mobil
		});

		return newUser.save().then((userCreated ,err) => {
			
			if (userCreated) {
				return userCreated;
			} else {
				return `Failed to Register a new Account`
			};
		});

	};

	//[SUB-SECTION] [USER AUTHENTICATION]

	module.exports.loginUser = (userData) => {
	
		let email = userData.email;
		let passW = userData.password;
		//upon executing this task a promise will be created
		return User.findOne({email: email}).then(result => {
			
			if (result === null) {
				return 'Email Not Found';
			} else {
				return 'Email Found'
			}
		})
	}

	//[SUB-SECTION] [LOGIN FUNCTION]

	module.exports.loginUser = (req, res) => {

			console.table(req.body)

			User.findOne({email: req.body.email}).then(foundUser => {
				if(foundUser === null){
					return res.send(`User Not Found`)
				} else {
					const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

					console.log(isPasswordCorrect)

					if(isPasswordCorrect) {
						return res.send({accessToken: auth.createAccessToken(foundUser)})
					} else {
						return res.send("Incorrect Password")
					}
					
				}
			}).catch(err => res.send(err))
		};		



	//[SUB-SECTION] [ENROLL]

	module.exports.enroll = async (req, res) => {
		console.log(req.user.id)
		console.log(req.body.courseId)

		if(req.user.isAdmin) {
			return res.send("Action Forbidden")
		}

		//ADDING COURSE TO USER

		let isUserUpdated = await User.findById(req.user.id).then(user => {

			let newEnrollment = {
				courseId: req.body.courseId
			}
	
			user.enrollments.push(newEnrollment);
			return user.save().then(course => true).catch(err => err.message)

		})

		
		if(isUserUpdated !== true) {
			return res.send({message: isUserUpdated})
		}

		//ADDING USER TO COURSE

		let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

			let enrollee = {
				userId: req.user.id
			}

			course.enrollees.push(enrollee);

			return course.save().then(course => true).catch(err => err.message)
		})

		if(isCourseUpdated !== true) {
			return res.send({message: isCourseUpdated})
		}

		if(isUserUpdated && isCourseUpdated) {
			return res.send({message: "Enrolled Successfully."})
		}


	}

	

//[SECTION] Functionalities [RETRIEVE]

	//[SUB-SECTION] [GET USER'S DETAILS]

	module.exports.getUserDetails = (req, res) => {

		User.findById(req.user.id).then(result => res.send(result)).catch(err => res.send(err));
	};


	//[SUB-SECTION] [GET USER'S ENROLLED SUBJECT]

	module.exports.getEnrollments = (req, res) => {
		User.findById(req.user.id).then(result => res.send(result.enrollments)).catch(err => res.send(err))
	};

//[SECTION] Functionalities [UPDATE]

//[SECTION] Functionalities [DELETE]