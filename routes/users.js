//[SECTION] Dependencies and Modules
	
	const exp = require("express");
	const controller = require('../controllers/users');
	const auth = require("../auth");
	const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
	
	const route = exp.Router()

//[SECTION] Routes [POST]

	//[SUB-SECTION] [CREATE USER]
	
	route.post('/register',(req, res) => {
		
		let userDetails = req.body;
		controller.registerUser(userDetails).then(outcome => {
			res.send(outcome);
		});
	});

	//[SUB-SECTION] [USER AUTHENTICATION]

		//[USER AUTHENTICATION] [LOGIN]

	route.post("/login", controller.loginUser);

		//[USER AUTHENTICATION] [ENROLL]

	route.post("/enroll", verify, controller.enroll)

		

//[SECTION] Routes [GET]

		//[USER AUTHENTICATION] [GET USER'S DETAILS]

	route.get("/getUserDetails", verify, controller.getUserDetails);

		//[USER AUTHENTICATION] [GET USER'S ENROLLED SUBJECT]

	route.get("/getEnrollments", verify, controller.getEnrollments)

//[SECTION] Routes [PUT]
//[SECTION] Routes [DEL]
//[SECTION] Expose Route System
	
	module.exports = route;