//[SECTION] Dependencies and Modules
	const exp = require("express");
	const controller = require('../controllers/courses');
	const auth = require("../auth");
	const {verify, verifyAdmin} = auth;
	
//[SECTION] Routing Component
	
	const route = exp.Router();

//[SECTION] [POST] Routes
	
	route.post('/create', verify, verifyAdmin, (req, res) => {
		let data = req.body;
		controller.createCourse(data).then(outcome => {
			res.send(outcome);
		})
	})

//[SECTION] [GET] Routes

	//[SUB-SECTION] [GET ALL COURSE]
	
		route.get('/all', verify, verifyAdmin, (req, res) => {
			
			controller.getAllCourse().then(outcome => {
				
				res.send(outcome)
			})
		});

	//[SUB-SECTION] [GET SINGLE COURSE]
	
		route.get('/:id', (req, res) => {
			let courseId = req.params.id;

			controller.getCourse(courseId).then(result => {
				
				res.send(result);
			})
		})

	//[SUB-SECTION] [GET ALL ACTIVE COURSE]
	
		route.get('/', (req, res) => {
			
			controller.getAllActiveCourses().then(outcome => {
				
				res.send(outcome);
			});
		});
	
//[SECTION] [PUT] Routes
	
	//[SUB-SECTION] [UPDATE COURSE]

		route.put('/:id', verify, verifyAdmin, (req, res) => {

			let id = req.params.id;
			let details = req.body;

			let cName = details.name;
			let cDesc = details.description;
			let cCost = details.price;

			if (cName !== '' && cDesc !== '' &&  cCost !== '') {

				controller.updateCourse(id, details).then(outcome => {
				res.send(outcome);
			});

			} else {
				res.send('Incorrect Input, Make sure details are complete')
			}

		});

	//[SUB-SECTION] [DEACTIVATE COURSE]

		route.put('/:id/archive', verify, verifyAdmin, (req, res) => {
			
			let courseId = req.params.id;
			
			controller.deactivateCourse(courseId).then(resultOfTheFunction => {
				
				res.send(resultOfTheFunction);
			})
		});

	//[SUB-SECTION] [REACTIVATE COURSE] [**ACTIVITY**]

		route.put('/:id/reactivate', verify, verifyAdmin, (req, res) => {
			
			let courseId = req.params.id;
			
			controller.reactivateCourse(courseId).then(resultOfTheFunction => {
				
				res.send(resultOfTheFunction);
			})
		});

//[SECTION] [DELETE] Routes
	
	route.delete('/:id', verify, verifyAdmin, (req, res) => {
	
		let id = req.params.id;
		
		controller.deleteCourse(id).then(outcome => {
			res.send(outcome);
		})
	});

//[SECTION] Export Route System
	
	module.exports = route;